#!/bin/bash

go install
export PATH=$PATH:$(dirname $(go list -f '{{.Target}}' .))
gocrawl